Name:           plexus-velocity
Version:        1.2
Release:        6
Summary:        Plexus Velocity Component
License:        ASL 2.0
URL:            https://codehaus-plexus.github.io/plexus-velocity/
BuildArch:      noarch

Source0:        https://github.com/codehaus-plexus/%{name}/archive/%{name}-%{version}.tar.gz
Source1:        http://www.apache.org/licenses/LICENSE-2.0.txt

BuildRequires:  maven-local mvn(commons-collections:commons-collections) mvn(org.codehaus.plexus:plexus-components:pom:)
BuildRequires:  mvn(org.codehaus.plexus:plexus-container-default) mvn(velocity:velocity)

%description
This package provides Plexus Velocity component - a wrapper for
Apache Velocity template engine, which allows easy use of Velocity
by applications built on top of Plexus container.

%package javadoc
Summary:        API documentation for %{name}

%description javadoc
This package provides %{summary}.

%prep
%autosetup -n %{name}-%{name}-%{version} -p1

find -name '*.jar' -delete

cp -p %{SOURCE1} LICENSE

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE

%files javadoc -f .mfiles-javadoc
%license LICENSE

%changelog
* Wed Feb 19 2020 chenli <chenli147@huawei.com> - 0:1.2-6
- Init Package
